﻿using CyberEye.Models;
using CyberEye.Models.OFAC;
using CyberEye.Models.ViewItem;
using OfficeOpenXml;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace CyberEye.Controllers
{
    public class HomeController : Controller
    {
        private const int _MaxLow = 24;
        private const int _MaxMedium = 27;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ImportOfac()
        {
            return View();
        }

        //https://www.treasury.gov/ofac/downloads/sdn.xml
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ImportOfac(HttpPostedFileBase inputFile)
        {
            if (inputFile != null && inputFile.ContentLength > 0)
            {
                using (StreamReader reader = new StreamReader(inputFile.InputStream))
                {
                    System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(sdnList));
                    var data = (sdnList)serializer.Deserialize(reader);
                    List<CommonData> list = new List<CommonData>();
                    foreach (var item in data.sdnEntry)
                    {
                        string name = item.firstName + "  " + item.lastName;
                        string _dob = "";
                        string address = "";
                        string _occupation = item.title;
                        string _id = "";

                        if (item.dateOfBirthList != null && item.dateOfBirthList.dateOfBirthItem != null)
                        {
                            _dob = item.dateOfBirthList.dateOfBirthItem.dateOfBirth;
                        }

                        if (item.placeOfBirthList != null && item.placeOfBirthList.placeOfBirthItem != null)
                        {
                            address = item.placeOfBirthList.placeOfBirthItem.placeOfBirth;
                        }

                        if (item.idList != null && item.idList.id != null)
                        {
                            _id = item.idList.id.idNumber;
                        }


                        //if(item.ite)


                        //CommonData person = new CommonData();
                        //person.Name = String.Format("{0} {1}", item.FIRST_NAME, item.SECOND_NAME);
                        //if (item.INDIVIDUAL_DATE_OF_BIRTH.Length > 0)
                        //{
                        //    if (item.INDIVIDUAL_DATE_OF_BIRTH[0].DATE.ToString("dd/MM/yyyy") != "01-01-0001")
                        //    {
                        //        person.DOB = item.INDIVIDUAL_DATE_OF_BIRTH[0].DATE.ToString("dd/MM/yyyy");
                        //    }
                        //}
                        //person.Nationlity = item.NATIONALITY != null ? item.NATIONALITY.VALUE : "-";
                        //person.Address = "";
                        //person.Propose_of_Transaction = "";
                        //person.CustomerID = "";

                        //if (item.INDIVIDUAL_DOCUMENT.Length > 0)
                        //{
                        //    if (!string.IsNullOrEmpty(item.INDIVIDUAL_DOCUMENT[0].TYPE_OF_DOCUMENT))
                        //    {
                        //        person.CustomerID = item.INDIVIDUAL_DOCUMENT[0].NUMBER;
                        //    }
                        //}

                        //person.SourceOfIncome = "";
                        //person.CountryOfResidence = "";
                        //person.Occupation = "";
                        //person.ContactNo = "";

                        CommonData person = new CommonData { Name = name, DOB = _dob, Address = address, CustomerID = _id, Occupation = _occupation };
                        if (!string.IsNullOrEmpty(_dob))
                            list.Add(person);
                        //List<string> ss = new List<string>();
                        //foreach (var point in item.INDIVIDUALS) ss.Add(point.X + "," + point.Y);
                        //list.Add(string.Join(",", ss));
                    }

                    string csvfile = CsvSerializer.SerializeToCsv(list);
                    System.IO.File.WriteAllText(Server.MapPath("~/Files" + "/ofac.csv"), csvfile);
                    ViewBag.Message = "Import Success";
                    return View(data);
                    //return File(Encoding.ASCII.GetBytes(csv), "text/csv");
                }
            }
            return View();
        }

        public ActionResult ImportSanction()
        {
            return View();
        }

        // https://www.un.org/sc/suborg/en/sanctions/un-sc-consolidated-list
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ImportSanction(HttpPostedFileBase inputFile)
        {
            if (inputFile != null && inputFile.ContentLength > 0)
            {
                using (StreamReader reader = new StreamReader(inputFile.InputStream))
                {
                    System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(CONSOLIDATED_LIST));
                    var data = (CONSOLIDATED_LIST)serializer.Deserialize(reader);
                    List<CommonData> list = new List<CommonData>();
                    foreach (var item in data.INDIVIDUALS)
                    {
                        CommonData person = new CommonData();
                        person.Name = String.Format("{0} {1}", item.FIRST_NAME, item.SECOND_NAME);
                        if (item.INDIVIDUAL_DATE_OF_BIRTH.Length > 0)
                        {
                            if (item.INDIVIDUAL_DATE_OF_BIRTH[0].DATE.ToString("dd/MM/yyyy") != "01-01-0001")
                            {
                                person.DOB = item.INDIVIDUAL_DATE_OF_BIRTH[0].DATE.ToString("dd/MM/yyyy");
                            }
                        }
                        person.Nationlity = item.NATIONALITY != null ? item.NATIONALITY.VALUE : "-";
                        person.Address = "";
                        person.Propose_of_Transaction = "";
                        person.CustomerID = "";

                        if (item.INDIVIDUAL_DOCUMENT.Length > 0)
                        {
                            if (!string.IsNullOrEmpty(item.INDIVIDUAL_DOCUMENT[0].TYPE_OF_DOCUMENT))
                            {
                                person.CustomerID = item.INDIVIDUAL_DOCUMENT[0].NUMBER;
                            }
                        }

                        person.SourceOfIncome = "";
                        person.CountryOfResidence = "";
                        person.Occupation = "";
                        person.ContactNo = "";

                        list.Add(person);
                        //List<string> ss = new List<string>();
                        //foreach (var point in item.INDIVIDUALS) ss.Add(point.X + "," + point.Y);
                        //list.Add(string.Join(",", ss));
                    }

                    string csvFile = CsvSerializer.SerializeToCsv(list);
                    System.IO.File.WriteAllText(Server.MapPath("~/Files" + "/sanction.csv"), csvFile);
                    //Fill.
                    //return View(data);
                    //return File(Encoding.ASCII.GetBytes(csvFile), "text/csv");
                    ViewBag.Message = "Upload success.";
                    return View(data);
                }
            }
            return View();
        }

        public ActionResult ImportBigData()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ImportBigData(HttpPostedFileBase inputFile)
        {
            if (inputFile != null && inputFile.ContentLength > 0)
            {
                inputFile.SaveAs(Server.MapPath("~/Files/BigData.xlsx"));
                ViewBag.Message = "Imported Success";
            }
            return View();
        }

        public ActionResult RBAProfile()
        {
            using (var package = new ExcelPackage(new FileInfo(Server.MapPath("~/Files/BigData.xlsx"))))
            {
                if (package.Workbook.Worksheets != null && package.Workbook.Worksheets.Count > 0)
                {
                    List<CommonData> item_list = GetDataFromExcelBigData(package.Workbook.Worksheets[1]);

                    ViewChartLevel chartBar = new ViewChartLevel();
                    //chartBar.Low = item_list.Where(a => a.RBAProfile.TotalScore >= 0 && a.RBAProfile.TotalScore <= 25).Count();
                    //chartBar.Medium = item_list.Where(a => a.RBAProfile.TotalScore >= 26 && a.RBAProfile.TotalScore <= 50).Count();
                    //chartBar.Height = item_list.Where(a => a.RBAProfile.TotalScore >= 51).Count();
                    chartBar.Low = item_list.Where(a => a.RBAProfile.TotalScore >= 0 && a.RBAProfile.TotalScore <= _MaxLow).Count();
                    chartBar.Medium = item_list.Where(a => a.RBAProfile.TotalScore >= _MaxLow + 1 && a.RBAProfile.TotalScore <= _MaxMedium).Count();
                    chartBar.Height = item_list.Where(a => a.RBAProfile.TotalScore > _MaxMedium).Count();
                    return View(chartBar);
                }
            }
            return View();
        }

        public ActionResult PatternAnalysis()
        {
            using (var package = new ExcelPackage(new FileInfo(Server.MapPath("~/Files/BigData.xlsx"))))
            {
                if (package.Workbook.Worksheets != null && package.Workbook.Worksheets.Count > 0)
                {
                    List<CommonData> item_list = GetDataFromExcelBigData(package.Workbook.Worksheets[1]).Where(a => a.RBAProfile.TotalScore > _MaxMedium).ToList();

                    //ViewChartLevel chartBar = new ViewChartLevel();
                    ////chartBar.Low = item_list.Where(a => a.RBAProfile.TotalScore >= 0 && a.RBAProfile.TotalScore <= 25).Count();
                    ////chartBar.Medium = item_list.Where(a => a.RBAProfile.TotalScore >= 26 && a.RBAProfile.TotalScore <= 50).Count();
                    ////chartBar.Height = item_list.Where(a => a.RBAProfile.TotalScore >= 51).Count();
                    //chartBar.Low = item_list.Where(a => a.RBAProfile.TotalScore >= 0 && a.RBAProfile.TotalScore <= 6).Count();
                    //chartBar.Medium = item_list.Where(a => a.RBAProfile.TotalScore >= 7 && a.RBAProfile.TotalScore <= 20).Count();
                    //chartBar.Height = item_list.Where(a => a.RBAProfile.TotalScore >= 21).Count();



                    return View(item_list);
                }
            }
            return View();
        }

        public ActionResult DisplayRiskProfiling_RBA()
        {

            using (var package = new ExcelPackage(new FileInfo(Server.MapPath("~/Files/BigData.xlsx"))))
            {
                if (package.Workbook.Worksheets != null && package.Workbook.Worksheets.Count > 0)
                {
                    List<CommonData> item_list = GetDataFromExcelBigData(package.Workbook.Worksheets[1]);

                    return View(item_list);
                }
            }

            return View();
        }

        public PartialViewResult ShowPartialRiskInfo(string name, string bod)
        {
            name = Server.UrlDecode(name);
            bod = Server.UrlDecode(bod);

            using (var package = new ExcelPackage(new FileInfo(Server.MapPath("~/Files/BigData.xlsx"))))
            {
                if (package.Workbook.Worksheets != null && package.Workbook.Worksheets.Count > 0)
                {
                    List<CommonData> item_list = GetDataFromExcelBigData(package.Workbook.Worksheets[1]);

                    CommonData result = item_list.FirstOrDefault(a => a.Name == name && a.DOB == bod);

                    if (result == null && bod == null)
                        result = item_list.FirstOrDefault(a => a.Name == name && a.DOB == string.Empty);

                    return PartialView("Partial/PartialRiskInfo", result);
                }
            }

            return PartialView();
        }

        private List<CommonData> GetDataFromExcelBigData(ExcelWorksheet sheet)
        {
            int firstDataRow = 2;
            int endRowNumber = sheet.Dimension.End.Row;
            int endColumns = sheet.Dimension.End.Column;

            List<CommonData> result = new List<CommonData>();

            var contents = System.IO.File.ReadAllText(Server.MapPath("~/Files/sanction.csv"));

            List<CommonData> data_sanction = ServiceStack.Text.CsvSerializer.DeserializeFromString<List<CommonData>>(contents);


            contents = System.IO.File.ReadAllText(Server.MapPath("~/Files/ofac.csv"));

            List<CommonData> data_ofac = ServiceStack.Text.CsvSerializer.DeserializeFromString<List<CommonData>>(contents);


            for (int row = firstDataRow; row <= endRowNumber; row++)
            {
                string name = GetCellStringValue(sheet, row, 3);
                string nationality = GetCellStringValue(sheet, row, 15);
                string country = GetCellStringValue(sheet, row, 21);
                string dateOfBirth = GetCellStringValue(sheet, row, 25);
                string occupation = GetCellStringValue(sheet, row, 16);
                string customerType = GetCellStringValue(sheet, row, 11);
                string transactionTimes = GetCellStringValue(sheet, row, 1);
                string _purchaseRMString = GetCellStringValue(sheet, row, 9);
                string _phone = GetCellStringValue(sheet, row, 22);

                Decimal purchaseRM = !string.IsNullOrEmpty(_purchaseRMString) ? decimal.Parse(_purchaseRMString) : 0m;

                if (!string.IsNullOrEmpty(name))
                {

                    CommonData data = new CommonData { Name = name, Nationlity = nationality, CountryOfResidence = country, DOB = dateOfBirth, Occupation = occupation, CustomerType = customerType, TransacTionTimes = transactionTimes, PurchaseRM = purchaseRM, Phone = _phone };


                    if (data_sanction.Any(a => a.Name == data.Name && a.DOB == data.DOB))
                    {
                        data.SanctionList = true;
                    }
                    else
                        data.SanctionList = false;

                    if (data_ofac.Any(a => a.Name == data.Name && a.DOB == data.DOB))
                    {
                        data.OfacData = true;
                    }
                    else data.OfacData = false;



                    data.RBAProfile = new RBAProfile(occupation, customerType, country, data);

                    result.Add(data);
                }
            }

            return result;
        }

        private string GetCellStringValue(ExcelWorksheet sheet, int row, int column)
        {
            if (sheet.Cells[row, column].Value != null)
            {
                return sheet.Cells[row, column].Value.ToString().Trim();
            }

            return String.Empty;
        }
    }
}