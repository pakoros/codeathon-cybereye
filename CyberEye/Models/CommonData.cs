﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CyberEye.Models
{
    public class CommonData
    {
        public string Name { get; set; }
        public string DOB { get; set; }
        public string Nationlity { get; set; }
        public string Address { get; set; }
        public string Propose_of_Transaction { get; set; }
        public string CustomerID { get; set; }
        public string SourceOfIncome { get; set; }
        public string CountryOfResidence { get; set; }
        public string Occupation { get; set; }
        public string ContactNo { get; set; }
        public string CustomerType { get; set; }
        public string TransacTionTimes { get; set; }
        public decimal PurchaseRM { get; set; }
        public string Phone { get; set; }

        public bool OfacData { get; set; }
        public bool SanctionList { get; set; }

        public RBAProfile RBAProfile { get; set; }
    }
}