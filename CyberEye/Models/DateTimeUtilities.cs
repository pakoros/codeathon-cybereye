﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CyberEye.Models
{
    public static class DateTimeUtilities
    {
        public static DateTime ConvertDateTime(string value)
        {
            string[] splittime = value.Split(' ');
            if (splittime.Length == 3)
            {
                string[] dateSplit = splittime[0].Split('/');
                string[] timeSplit = splittime[1].Split(':');

                return new DateTime(int.Parse(dateSplit[2]), int.Parse(dateSplit[1]), int.Parse(dateSplit[0]), int.Parse(timeSplit[0]), int.Parse(timeSplit[1]), int.Parse(timeSplit[2]));
            }

            return DateTime.MinValue;
        }
    }
}