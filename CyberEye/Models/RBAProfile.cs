﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CyberEye.Models
{
    public class RBAProfile
    {
        public RBAWho Who { get; set; }
        public RBAWhere Where { get; set; }
        public RBAWhen When { get; set; }
        public RBAWhy Why { get; set; }
        public RBAWhat What { get; set; }

        public RBAProfile(string occupation, string customerType, string country, CommonData data)
        {
            Who = new RBAWho();
            Where = new RBAWhere();
            When = new RBAWhen();
            Why = new RBAWhy();
            What = new RBAWhat();

            #region Who
            if (string.IsNullOrEmpty(occupation))
                Who.Who_Employment = 15;
            else
                Who.Who_Employment = 1;

            if (!string.IsNullOrEmpty(customerType))
            {
                if (customerType.ToUpper().Contains("INVIDUAL"))
                    Who.Who_CustomerType = 1;
                else
                    Who.Who_CustomerType = 5;
            }     
            else
                Who.Who_CustomerType = 15;

            if (data.OfacData)
            {
                Who.Who_Surveillance = 10;
            }
            if (data.SanctionList)
            {
                Who.Who_Surveillance = 10;
            }
            #endregion

            #region Where
            if (country.ToUpper() == "MALAYSIA")
                Where.IsLocal = 1;

            if (data.Occupation.ToUpper() == "STUDENT")
            {
                Where.WhereForeigner = 15;
            }

            #endregion

            #region When

            DateTime date = DateTimeUtilities.ConvertDateTime(data.TransacTionTimes);
            if (date <= new DateTime(date.Year, date.Month, date.Day, 18, 0, 0))
            {
                When.TransactionRiskTime = 1;
            }
            else
            {
                When.TransactionRiskTime = 5;
            }

            if (date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday)
            {
                When.TransactionRiskTime += 5;
            }
            else
            {
                When.TransactionRiskTime += 1;
            }

            #endregion

            #region Why
            if (data.PurchaseRM < 10000)
            {
                Why.TransactionAmountRisk = 1;
            }
            else if (data.PurchaseRM <= 50000)
                Why.TransactionAmountRisk = 5;
            else
                Why.TransactionAmountRisk = 10;

            #endregion

            #region What

            #endregion
        }

        public int TotalScore
        {
            get
            {
                return Who.GetValue + Where.GetValue + When.GetValue + Why.GetValue + What.GetValue;
            }
        }
    }

    public class RBAWho
    {
        public int Who_Employment { get; set; }

        public int Who_CustomerType { get; set; }

        public int Who_LegalEntities { get; set; }

        public int Who_NatureOfBusiness { get; set; }

        public int Who_Surveillance { get; set; }

        public int Who_Profile { get; set; }

        public int GetValue
        {
            get
            {
                return Who_Employment + Who_CustomerType + Who_LegalEntities + Who_NatureOfBusiness + Who_Surveillance + Who_Profile;
            }
        }
    }

    //public enum Who_Employment
    //{
    //    General_Worker = 1,
    //    Management_Professional = 5,
    //    Business = 10,
    //    Unemployment = 15
    //}

    //public enum Who_CustomerType
    //{
    //    Invidual = 1,
    //    NonFaceToFace = 5,
    //    NotGovernmentOrganization = 10,
    //    PEP = 15
    //}

    //public enum Who_LegalEntities
    //{
    //    Berhad = 1,

    //    Freelance = 25,
    //    ShellCompany = 35
    //}

    //public enum Who_NatureOfBusiness
    //{
    //    RelateWithTransaction = 1,
    //    Unknow_NotClear = 5,
    //    NotRelateWithTransaction = 15
    //}

    //public enum Who_Surveillance
    //{
    //    None = 1,
    //    OnGoingList = 5,
    //    Watchlist_BlockList_BNMList_Other = 10
    //}

    //public enum Who_Profile
    //{
    //    Match = 1,
    //    Not_Match = 15
    //}

    public class RBAWhere
    {
        public int IsLocal { get; set; }
        public int WhereForeigner { get; set; }
        public int VisaValidity { get; set; }
        public int Address { get; set; }
        public int LocationRisk { get; set; }

        public int GetValue
        {
            get
            {
                return IsLocal + WhereForeigner + VisaValidity + Address + LocationRisk;
            }
        }
    }

    public class WhereForeigner
    {
        public int Passport { get; set; }
        public int PremanentResident { get; set; }
        public int CountryRisk { get; set; }
        public int GetValue
        {
            get
            {
                return Passport + PremanentResident + CountryRisk;
            }
        }
    }

    public class RBAWhen
    {
        public int FrequencyPerDay { get; set; }
        public int TransactionRiskTime { get; set; }
        public int SeasonLocal { get; set; }
        public int FrequencyRisk { get; set; }

        public int GetValue { get { return FrequencyPerDay + TransactionRiskTime + SeasonLocal + FrequencyRisk; } }
    }

    public class RBAWhy
    {
        public int TransactionAmountRisk
        {
            get;
            set;
        }
        public int TransactionFrequencyRisk { get; set; }

        public int GetValue { get { return TransactionAmountRisk + TransactionFrequencyRisk; } }
    }

    public class RBAWhat
    {
        public int SupportingDocuments { get; set; }
        public int AnnualIncome { get; set; }
        public int IncomeStatus { get; set; }
        public int SuspiciousActivity { get; set; }
        public int GetValue { get { return SupportingDocuments + AnnualIncome + IncomeStatus + SuspiciousActivity; } }
    }

}