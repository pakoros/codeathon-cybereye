﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CyberEye.Models
{
    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class CONSOLIDATED_LIST
    {

        private CONSOLIDATED_LISTINDIVIDUAL[] iNDIVIDUALSField;

        private CONSOLIDATED_LISTENTITY[] eNTITIESField;

        private System.DateTime dateGeneratedField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("INDIVIDUAL", IsNullable = false)]
        public CONSOLIDATED_LISTINDIVIDUAL[] INDIVIDUALS
        {
            get
            {
                return this.iNDIVIDUALSField;
            }
            set
            {
                this.iNDIVIDUALSField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ENTITY", IsNullable = false)]
        public CONSOLIDATED_LISTENTITY[] ENTITIES
        {
            get
            {
                return this.eNTITIESField;
            }
            set
            {
                this.eNTITIESField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public System.DateTime dateGenerated
        {
            get
            {
                return this.dateGeneratedField;
            }
            set
            {
                this.dateGeneratedField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSOLIDATED_LISTINDIVIDUAL
    {

        private uint dATAIDField;

        private byte vERSIONNUMField;

        private string fIRST_NAMEField;

        private string sECOND_NAMEField;

        private string tHIRD_NAMEField;

        private string fOURTH_NAMEField;

        private string uN_LIST_TYPEField;

        private string rEFERENCE_NUMBERField;

        private System.DateTime lISTED_ONField;

        private string nAME_ORIGINAL_SCRIPTField;

        private string cOMMENTS1Field;

        private string[] dESIGNATIONField;

        private CONSOLIDATED_LISTINDIVIDUALNATIONALITY nATIONALITYField;

        private CONSOLIDATED_LISTINDIVIDUALLIST_TYPE lIST_TYPEField;

        private string[] lAST_DAY_UPDATEDField;

        private CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_ALIAS[] iNDIVIDUAL_ALIASField;

        private CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_ADDRESS iNDIVIDUAL_ADDRESSField;

        private CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_DATE_OF_BIRTH[] iNDIVIDUAL_DATE_OF_BIRTHField;

        private CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_PLACE_OF_BIRTH iNDIVIDUAL_PLACE_OF_BIRTHField;

        private CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_DOCUMENT[] iNDIVIDUAL_DOCUMENTField;

        private object sORT_KEYField;

        private object sORT_KEY_LAST_MODField;

        /// <remarks/>
        public uint DATAID
        {
            get
            {
                return this.dATAIDField;
            }
            set
            {
                this.dATAIDField = value;
            }
        }

        /// <remarks/>
        public byte VERSIONNUM
        {
            get
            {
                return this.vERSIONNUMField;
            }
            set
            {
                this.vERSIONNUMField = value;
            }
        }

        /// <remarks/>
        public string FIRST_NAME
        {
            get
            {
                return this.fIRST_NAMEField;
            }
            set
            {
                this.fIRST_NAMEField = value;
            }
        }

        /// <remarks/>
        public string SECOND_NAME
        {
            get
            {
                return this.sECOND_NAMEField;
            }
            set
            {
                this.sECOND_NAMEField = value;
            }
        }

        /// <remarks/>
        public string THIRD_NAME
        {
            get
            {
                return this.tHIRD_NAMEField;
            }
            set
            {
                this.tHIRD_NAMEField = value;
            }
        }

        /// <remarks/>
        public string FOURTH_NAME
        {
            get
            {
                return this.fOURTH_NAMEField;
            }
            set
            {
                this.fOURTH_NAMEField = value;
            }
        }

        /// <remarks/>
        public string UN_LIST_TYPE
        {
            get
            {
                return this.uN_LIST_TYPEField;
            }
            set
            {
                this.uN_LIST_TYPEField = value;
            }
        }

        /// <remarks/>
        public string REFERENCE_NUMBER
        {
            get
            {
                return this.rEFERENCE_NUMBERField;
            }
            set
            {
                this.rEFERENCE_NUMBERField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime LISTED_ON
        {
            get
            {
                return this.lISTED_ONField;
            }
            set
            {
                this.lISTED_ONField = value;
            }
        }

        /// <remarks/>
        public string NAME_ORIGINAL_SCRIPT
        {
            get
            {
                return this.nAME_ORIGINAL_SCRIPTField;
            }
            set
            {
                this.nAME_ORIGINAL_SCRIPTField = value;
            }
        }

        /// <remarks/>
        public string COMMENTS1
        {
            get
            {
                return this.cOMMENTS1Field;
            }
            set
            {
                this.cOMMENTS1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("VALUE", IsNullable = false)]
        public string[] DESIGNATION
        {
            get
            {
                return this.dESIGNATIONField;
            }
            set
            {
                this.dESIGNATIONField = value;
            }
        }

        /// <remarks/>
        public CONSOLIDATED_LISTINDIVIDUALNATIONALITY NATIONALITY
        {
            get
            {
                return this.nATIONALITYField;
            }
            set
            {
                this.nATIONALITYField = value;
            }
        }

        /// <remarks/>
        public CONSOLIDATED_LISTINDIVIDUALLIST_TYPE LIST_TYPE
        {
            get
            {
                return this.lIST_TYPEField;
            }
            set
            {
                this.lIST_TYPEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("VALUE", IsNullable = false)]
        public string[] LAST_DAY_UPDATED
        {
            get
            {
                return this.lAST_DAY_UPDATEDField;
            }
            set
            {
                this.lAST_DAY_UPDATEDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("INDIVIDUAL_ALIAS")]
        public CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_ALIAS[] INDIVIDUAL_ALIAS
        {
            get
            {
                return this.iNDIVIDUAL_ALIASField;
            }
            set
            {
                this.iNDIVIDUAL_ALIASField = value;
            }
        }

        /// <remarks/>
        public CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_ADDRESS INDIVIDUAL_ADDRESS
        {
            get
            {
                return this.iNDIVIDUAL_ADDRESSField;
            }
            set
            {
                this.iNDIVIDUAL_ADDRESSField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("INDIVIDUAL_DATE_OF_BIRTH")]
        public CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_DATE_OF_BIRTH[] INDIVIDUAL_DATE_OF_BIRTH
        {
            get
            {
                return this.iNDIVIDUAL_DATE_OF_BIRTHField;
            }
            set
            {
                this.iNDIVIDUAL_DATE_OF_BIRTHField = value;
            }
        }

        /// <remarks/>
        public CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_PLACE_OF_BIRTH INDIVIDUAL_PLACE_OF_BIRTH
        {
            get
            {
                return this.iNDIVIDUAL_PLACE_OF_BIRTHField;
            }
            set
            {
                this.iNDIVIDUAL_PLACE_OF_BIRTHField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("INDIVIDUAL_DOCUMENT")]
        public CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_DOCUMENT[] INDIVIDUAL_DOCUMENT
        {
            get
            {
                return this.iNDIVIDUAL_DOCUMENTField;
            }
            set
            {
                this.iNDIVIDUAL_DOCUMENTField = value;
            }
        }

        /// <remarks/>
        public object SORT_KEY
        {
            get
            {
                return this.sORT_KEYField;
            }
            set
            {
                this.sORT_KEYField = value;
            }
        }

        /// <remarks/>
        public object SORT_KEY_LAST_MOD
        {
            get
            {
                return this.sORT_KEY_LAST_MODField;
            }
            set
            {
                this.sORT_KEY_LAST_MODField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSOLIDATED_LISTINDIVIDUALNATIONALITY
    {

        private string vALUEField;

        /// <remarks/>
        public string VALUE
        {
            get
            {
                return this.vALUEField;
            }
            set
            {
                this.vALUEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSOLIDATED_LISTINDIVIDUALLIST_TYPE
    {

        private string vALUEField;

        /// <remarks/>
        public string VALUE
        {
            get
            {
                return this.vALUEField;
            }
            set
            {
                this.vALUEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_ALIAS
    {

        private string qUALITYField;

        private string aLIAS_NAMEField;

        /// <remarks/>
        public string QUALITY
        {
            get
            {
                return this.qUALITYField;
            }
            set
            {
                this.qUALITYField = value;
            }
        }

        /// <remarks/>
        public string ALIAS_NAME
        {
            get
            {
                return this.aLIAS_NAMEField;
            }
            set
            {
                this.aLIAS_NAMEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_ADDRESS
    {

        private string cOUNTRYField;

        private string nOTEField;

        /// <remarks/>
        public string COUNTRY
        {
            get
            {
                return this.cOUNTRYField;
            }
            set
            {
                this.cOUNTRYField = value;
            }
        }

        /// <remarks/>
        public string NOTE
        {
            get
            {
                return this.nOTEField;
            }
            set
            {
                this.nOTEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_DATE_OF_BIRTH
    {

        private string tYPE_OF_DATEField;

        private ushort yEARField;

        private bool yEARFieldSpecified;

        private System.DateTime dATEField;

        private bool dATEFieldSpecified;

        /// <remarks/>
        public string TYPE_OF_DATE
        {
            get
            {
                return this.tYPE_OF_DATEField;
            }
            set
            {
                this.tYPE_OF_DATEField = value;
            }
        }

        /// <remarks/>
        public ushort YEAR
        {
            get
            {
                return this.yEARField;
            }
            set
            {
                this.yEARField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool YEARSpecified
        {
            get
            {
                return this.yEARFieldSpecified;
            }
            set
            {
                this.yEARFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime DATE
        {
            get
            {
                return this.dATEField;
            }
            set
            {
                this.dATEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DATESpecified
        {
            get
            {
                return this.dATEFieldSpecified;
            }
            set
            {
                this.dATEFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_PLACE_OF_BIRTH
    {

        private string cOUNTRYField;

        /// <remarks/>
        public string COUNTRY
        {
            get
            {
                return this.cOUNTRYField;
            }
            set
            {
                this.cOUNTRYField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSOLIDATED_LISTINDIVIDUALINDIVIDUAL_DOCUMENT
    {

        private string tYPE_OF_DOCUMENTField;

        private string nUMBERField;

        private string nOTEField;

        /// <remarks/>
        public string TYPE_OF_DOCUMENT
        {
            get
            {
                return this.tYPE_OF_DOCUMENTField;
            }
            set
            {
                this.tYPE_OF_DOCUMENTField = value;
            }
        }

        /// <remarks/>
        public string NUMBER
        {
            get
            {
                return this.nUMBERField;
            }
            set
            {
                this.nUMBERField = value;
            }
        }

        /// <remarks/>
        public string NOTE
        {
            get
            {
                return this.nOTEField;
            }
            set
            {
                this.nOTEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSOLIDATED_LISTENTITY
    {

        private uint dATAIDField;

        private byte vERSIONNUMField;

        private string fIRST_NAMEField;

        private string uN_LIST_TYPEField;

        private string rEFERENCE_NUMBERField;

        private System.DateTime lISTED_ONField;

        private string nAME_ORIGINAL_SCRIPTField;

        private string cOMMENTS1Field;

        private CONSOLIDATED_LISTENTITYLIST_TYPE lIST_TYPEField;

        private string[] lAST_DAY_UPDATEDField;

        private CONSOLIDATED_LISTENTITYENTITY_ALIAS[] eNTITY_ALIASField;

        private CONSOLIDATED_LISTENTITYENTITY_ADDRESS[] eNTITY_ADDRESSField;

        private object sORT_KEYField;

        private object sORT_KEY_LAST_MODField;

        /// <remarks/>
        public uint DATAID
        {
            get
            {
                return this.dATAIDField;
            }
            set
            {
                this.dATAIDField = value;
            }
        }

        /// <remarks/>
        public byte VERSIONNUM
        {
            get
            {
                return this.vERSIONNUMField;
            }
            set
            {
                this.vERSIONNUMField = value;
            }
        }

        /// <remarks/>
        public string FIRST_NAME
        {
            get
            {
                return this.fIRST_NAMEField;
            }
            set
            {
                this.fIRST_NAMEField = value;
            }
        }

        /// <remarks/>
        public string UN_LIST_TYPE
        {
            get
            {
                return this.uN_LIST_TYPEField;
            }
            set
            {
                this.uN_LIST_TYPEField = value;
            }
        }

        /// <remarks/>
        public string REFERENCE_NUMBER
        {
            get
            {
                return this.rEFERENCE_NUMBERField;
            }
            set
            {
                this.rEFERENCE_NUMBERField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime LISTED_ON
        {
            get
            {
                return this.lISTED_ONField;
            }
            set
            {
                this.lISTED_ONField = value;
            }
        }

        /// <remarks/>
        public string NAME_ORIGINAL_SCRIPT
        {
            get
            {
                return this.nAME_ORIGINAL_SCRIPTField;
            }
            set
            {
                this.nAME_ORIGINAL_SCRIPTField = value;
            }
        }

        /// <remarks/>
        public string COMMENTS1
        {
            get
            {
                return this.cOMMENTS1Field;
            }
            set
            {
                this.cOMMENTS1Field = value;
            }
        }

        /// <remarks/>
        public CONSOLIDATED_LISTENTITYLIST_TYPE LIST_TYPE
        {
            get
            {
                return this.lIST_TYPEField;
            }
            set
            {
                this.lIST_TYPEField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("VALUE", IsNullable = false)]
        public string[] LAST_DAY_UPDATED
        {
            get
            {
                return this.lAST_DAY_UPDATEDField;
            }
            set
            {
                this.lAST_DAY_UPDATEDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ENTITY_ALIAS")]
        public CONSOLIDATED_LISTENTITYENTITY_ALIAS[] ENTITY_ALIAS
        {
            get
            {
                return this.eNTITY_ALIASField;
            }
            set
            {
                this.eNTITY_ALIASField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ENTITY_ADDRESS")]
        public CONSOLIDATED_LISTENTITYENTITY_ADDRESS[] ENTITY_ADDRESS
        {
            get
            {
                return this.eNTITY_ADDRESSField;
            }
            set
            {
                this.eNTITY_ADDRESSField = value;
            }
        }

        /// <remarks/>
        public object SORT_KEY
        {
            get
            {
                return this.sORT_KEYField;
            }
            set
            {
                this.sORT_KEYField = value;
            }
        }

        /// <remarks/>
        public object SORT_KEY_LAST_MOD
        {
            get
            {
                return this.sORT_KEY_LAST_MODField;
            }
            set
            {
                this.sORT_KEY_LAST_MODField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSOLIDATED_LISTENTITYLIST_TYPE
    {

        private string vALUEField;

        /// <remarks/>
        public string VALUE
        {
            get
            {
                return this.vALUEField;
            }
            set
            {
                this.vALUEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSOLIDATED_LISTENTITYENTITY_ALIAS
    {

        private string qUALITYField;

        private string aLIAS_NAMEField;

        /// <remarks/>
        public string QUALITY
        {
            get
            {
                return this.qUALITYField;
            }
            set
            {
                this.qUALITYField = value;
            }
        }

        /// <remarks/>
        public string ALIAS_NAME
        {
            get
            {
                return this.aLIAS_NAMEField;
            }
            set
            {
                this.aLIAS_NAMEField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class CONSOLIDATED_LISTENTITYENTITY_ADDRESS
    {

        private object[] itemsField;

        private ItemsChoiceType[] itemsElementNameField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("CITY", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("COUNTRY", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("NOTE", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("STATE_PROVINCE", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("STREET", typeof(string))]
        [System.Xml.Serialization.XmlElementAttribute("ZIP_CODE", typeof(ushort))]
        [System.Xml.Serialization.XmlChoiceIdentifierAttribute("ItemsElementName")]
        public object[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ItemsElementName")]
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public ItemsChoiceType[] ItemsElementName
        {
            get
            {
                return this.itemsElementNameField;
            }
            set
            {
                this.itemsElementNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(IncludeInSchema = false)]
    public enum ItemsChoiceType
    {

        /// <remarks/>
        CITY,

        /// <remarks/>
        COUNTRY,

        /// <remarks/>
        NOTE,

        /// <remarks/>
        STATE_PROVINCE,

        /// <remarks/>
        STREET,

        /// <remarks/>
        ZIP_CODE,
    }
}