﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CyberEye.Models.OFAC
{
    [XmlRoot("sdnList")]
    public class sdnList
    {
        [XmlElement("sdnEntry")]
        public sdnEntry[] sdnEntry { get; set; }
    }


    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class sdnEntry
    {

        private ushort uidField;

        private string firstNameField;

        private string lastNameField;

        private string titleField;

        private string sdnTypeField;

        private sdnEntryProgramList programListField;

        private sdnEntryIdList idListField;

        private sdnEntryDateOfBirthList dateOfBirthListField;

        private sdnEntryPlaceOfBirthList placeOfBirthListField;

        /// <remarks/>
        public ushort uid
        {
            get
            {
                return this.uidField;
            }
            set
            {
                this.uidField = value;
            }
        }

        /// <remarks/>
        public string firstName
        {
            get
            {
                return this.firstNameField;
            }
            set
            {
                this.firstNameField = value;
            }
        }

        /// <remarks/>
        public string lastName
        {
            get
            {
                return this.lastNameField;
            }
            set
            {
                this.lastNameField = value;
            }
        }

        /// <remarks/>
        public string title
        {
            get
            {
                return this.titleField;
            }
            set
            {
                this.titleField = value;
            }
        }

        /// <remarks/>
        public string sdnType
        {
            get
            {
                return this.sdnTypeField;
            }
            set
            {
                this.sdnTypeField = value;
            }
        }

        /// <remarks/>
        public sdnEntryProgramList programList
        {
            get
            {
                return this.programListField;
            }
            set
            {
                this.programListField = value;
            }
        }

        /// <remarks/>
        public sdnEntryIdList idList
        {
            get
            {
                return this.idListField;
            }
            set
            {
                this.idListField = value;
            }
        }

        /// <remarks/>
        public sdnEntryDateOfBirthList dateOfBirthList
        {
            get
            {
                return this.dateOfBirthListField;
            }
            set
            {
                this.dateOfBirthListField = value;
            }
        }

        /// <remarks/>
        public sdnEntryPlaceOfBirthList placeOfBirthList
        {
            get
            {
                return this.placeOfBirthListField;
            }
            set
            {
                this.placeOfBirthListField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class sdnEntryProgramList
    {

        private string programField;

        /// <remarks/>
        public string program
        {
            get
            {
                return this.programField;
            }
            set
            {
                this.programField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class sdnEntryIdList
    {

        private sdnEntryIdListID idField;

        /// <remarks/>
        public sdnEntryIdListID id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class sdnEntryIdListID
    {

        private int uidField;

        private string idTypeField;

        private string idNumberField;

        private string idCountryField;

        /// <remarks/>
        public int uid
        {
            get
            {
                return this.uidField;
            }
            set
            {
                this.uidField = value;
            }
        }

        /// <remarks/>
        public string idType
        {
            get
            {
                return this.idTypeField;
            }
            set
            {
                this.idTypeField = value;
            }
        }

        /// <remarks/>
        public string idNumber
        {
            get
            {
                return this.idNumberField;
            }
            set
            {
                this.idNumberField = value;
            }
        }

        /// <remarks/>
        public string idCountry
        {
            get
            {
                return this.idCountryField;
            }
            set
            {
                this.idCountryField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class sdnEntryDateOfBirthList
    {

        private sdnEntryDateOfBirthListDateOfBirthItem dateOfBirthItemField;

        /// <remarks/>
        public sdnEntryDateOfBirthListDateOfBirthItem dateOfBirthItem
        {
            get
            {
                return this.dateOfBirthItemField;
            }
            set
            {
                this.dateOfBirthItemField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class sdnEntryDateOfBirthListDateOfBirthItem
    {

        private ushort uidField;

        private string dateOfBirthField;

        private bool mainEntryField;

        /// <remarks/>
        public ushort uid
        {
            get
            {
                return this.uidField;
            }
            set
            {
                this.uidField = value;
            }
        }

        /// <remarks/>
        public string dateOfBirth
        {
            get
            {
                return this.dateOfBirthField;
            }
            set
            {
                this.dateOfBirthField = value;
            }
        }

        /// <remarks/>
        public bool mainEntry
        {
            get
            {
                return this.mainEntryField;
            }
            set
            {
                this.mainEntryField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class sdnEntryPlaceOfBirthList
    {

        private sdnEntryPlaceOfBirthListPlaceOfBirthItem placeOfBirthItemField;

        /// <remarks/>
        public sdnEntryPlaceOfBirthListPlaceOfBirthItem placeOfBirthItem
        {
            get
            {
                return this.placeOfBirthItemField;
            }
            set
            {
                this.placeOfBirthItemField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class sdnEntryPlaceOfBirthListPlaceOfBirthItem
    {

        private ushort uidField;

        private string placeOfBirthField;

        private bool mainEntryField;

        /// <remarks/>
        public ushort uid
        {
            get
            {
                return this.uidField;
            }
            set
            {
                this.uidField = value;
            }
        }

        /// <remarks/>
        public string placeOfBirth
        {
            get
            {
                return this.placeOfBirthField;
            }
            set
            {
                this.placeOfBirthField = value;
            }
        }

        /// <remarks/>
        public bool mainEntry
        {
            get
            {
                return this.mainEntryField;
            }
            set
            {
                this.mainEntryField = value;
            }
        }
    }
}