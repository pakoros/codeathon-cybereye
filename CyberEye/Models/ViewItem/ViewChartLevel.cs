﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CyberEye.Models.ViewItem
{
    public class ViewChartLevel
    {
        public int Height { get; set; }
        public int Medium { get; set; }
        public int Low { get; set; }
    }
}