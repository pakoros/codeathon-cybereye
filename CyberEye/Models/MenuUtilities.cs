﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CyberEye.Models
{
    public static class MenuUtilities
    {
        public static string MenuIsActived(string action, string controller, string[] actionAccept, string[] controllerAccept, string classIfActived)
        {
            if (actionAccept.Contains(action, StringComparer.InvariantCultureIgnoreCase) && controllerAccept.Contains(controller, StringComparer.InvariantCultureIgnoreCase))
                return classIfActived;
            else
                return "";
        }
    }
}