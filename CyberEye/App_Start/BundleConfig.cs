﻿using System.Web;
using System.Web.Optimization;

namespace CyberEye
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/default.css"));

            bundles.Add(new StyleBundle("~/Content/fontcss").Include(
                    "~/Content/fontawesome-all.css"
                    ));

            // http://www.chartjs.org/
            bundles.Add(new ScriptBundle("~/ScriptsAddon/chartjs").Include(
                      "~/ScriptsAddon/Chart.bundle.js"));

            bundles.Add(new StyleBundle("~/Content/vali-admin/docs/css/css").Include(
                    "~/Content/vali-admin-2.3.0/docs/css/main.css"));

            bundles.Add(new ScriptBundle("~/bundles/vali-admin-js").Include(
                      "~/Content/vali-admin-2.3.0/docs/js/main.js"
                      ));
            bundles.Add(new ScriptBundle("~/bundles/jqueryajaxval").Include(
"~/Scripts/jquery.unobtrusive-ajax.js"));

        }
    }
}
